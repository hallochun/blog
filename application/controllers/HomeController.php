<?php

class HomeController extends CI_Controller
{
    public function __contruct()
    {
        parent::construct();
        $this->load->library('parser');
    }
    public function index()
    {
        $data = [
            'title' => 'Home',
        ];
        $this->parser->parse('contents/home', $data);
    }
}