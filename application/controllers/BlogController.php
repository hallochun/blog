<?php

class BlogController extends CI_Controller {

    public function index()
    {
        $data = [
            'title' => 'ini slug',
        ];
        $this->parser->parse('contents/blog', $data);
    }

    public function DetailBlog()
    {
        $data = [
            'title' => "All the world's waiting for you",
        ];
        // $this->load->view('templates/header', $data);
        $this->parser->parse('contents/detailBlog', $data);
        // $this->load->view('templates/footer', $data);
    }
}