<?php

class ProductController extends CI_Controller {

    public function index()
    {
        $data = [
            'title' => "List-Product",
        ];
        $this->parser->parse('contents/product', $data);
    }
}