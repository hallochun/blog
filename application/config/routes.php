<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'HomeController';


// blog
$route['blog'] = 'BlogController';
$route['detail/(:any)'] = 'BlogController/DetailBlog';

//product
$route['product'] = 'ProductController';


$route['404_override'] = 'HandleController';
$route['translate_uri_dashes'] = FALSE;
