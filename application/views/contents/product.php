<title>{title}</title>
<?php $this->load->view('/templates/header.php')?>
<div class="container" style="margin-top:100px">
    <h5 class="title-product-header mt-5">Our Product</h5>
    <hr class="accessory" />
    <div class="row">
        <div class="col-md-4">
            <div class="card mt-5 ml-2" style="width: 18rem;">
                <img class="card-img-top zoom" src="<?= base_url()?>__assets/img1.png" height="200px" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Aplikasi Monitoring Penggajian dan Karyawan Berbasis Web</p>
                    <hr>
                    <span class="badge badge-primary">By Hallochun</span><br>
                    <a target="_blank" class="btn btn-info btn-sm ml-4 mt-4" href="https://api.whatsapp.com/send?phone=+6281310191826&text=Teks">Chat</a>
                    <button class="btn btn-success btn-sm ml-3 mt-4">Demo</button>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card mt-5 ml-2" style="width: 18rem;">
                <img class="card-img-top zoom" src="<?= base_url()?>__assets/img1.png" height="200px" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Aplikasi Monitoring Penggajian dan Karyawan Berbasis Web</p>
                    <hr>
                    <span class="badge badge-primary">By Hallochun</span><br>
                    <button class="btn btn-info btn-sm ml-4 mt-4">Chat</button>
                    <button class="btn btn-success btn-sm ml-3 mt-4">Demo</button>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card mt-5 ml-2" style="width: 18rem;">
                <img class="card-img-top zoom" src="<?= base_url()?>__assets/img1.png" height="200px" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Aplikasi Monitoring Penggajian dan Karyawan Berbasis Web</p>
                    <hr>
                    <span class="badge badge-primary">By Hallochun</span><br>
                    <button class="btn btn-info btn-sm ml-4 mt-4">Chat</button>
                    <button class="btn btn-success btn-sm ml-3 mt-4">Demo</button>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card mt-5 ml-2" style="width: 18rem;">
                <img class="card-img-top zoom" src="<?= base_url()?>__assets/img1.png" height="200px" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Aplikasi Monitoring Penggajian dan Karyawan Berbasis Web</p>
                    <hr>
                    <span class="badge badge-primary">By Hallochun</span><br>
                    <button class="btn btn-info btn-sm ml-4 mt-4">Chat</button>
                    <button class="btn btn-success btn-sm ml-3 mt-4">Demo</button>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card mt-5 ml-2" style="width: 18rem;">
                <img class="card-img-top zoom" src="<?= base_url()?>__assets/img1.png" height="200px" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Aplikasi Monitoring Penggajian dan Karyawan Berbasis Web</p>
                    <hr>
                    <span class="badge badge-primary">By Hallochun</span><br>
                    <button class="btn btn-info btn-sm ml-4 mt-4">Chat</button>
                    <button class="btn btn-success btn-sm ml-3 mt-4">Demo</button>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card mt-5 ml-2" style="width: 18rem;">
                <img class="card-img-top zoom" src="<?= base_url()?>__assets/img1.png" height="200px" alt="Card image cap">
                <div class="card-body">
                    <p class="card-text">Aplikasi Monitoring Penggajian dan Karyawan Berbasis Web</p>
                    <hr>
                    <span class="badge badge-primary">By Hallochun</span><br>
                    <button class="btn btn-info btn-sm ml-4 mt-4">Chat</button>
                    <button class="btn btn-success btn-sm ml-3 mt-4">Demo</button>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $this->load->view('/templates/footer.php')?>
<script src="<?= base_url()?>__public/js/product.js"></script>