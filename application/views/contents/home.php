<title>{title}</title>
<style>
ul, li {
    text-decoration: none;
    list-style:none;
}
</style>
<?php $this->load->view('/templates/header.php')?>
<div class="container" style="margin-top:100px">
    <div class="row">
        <div class="col-md-12">
            <div class="single-item">
                <div><img src="<?= base_url()?>__assets/img/slider/slider.jpg" width="100%" height="400px" class="img-responsive" alt="Adi Chandra Setiawan"></div>
                <div><img src="<?= base_url()?>__assets/img/slider/slider1.jpg" width="100%" height="400px" class="img-responsive" alt="Adi Chandra Setiawan"></div>
                <div><img src="<?= base_url()?>__assets/img/slider/slider2.jpg" width="100%" height="400px" class="img-responsive" alt="Adi Chandra Setiawan"></div>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<!-- Blog -->
<div class="container mt-lg-5">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-4">
                    <div class="card mt-5">

                        <!-- Card image -->
                        <div class="view overlay">
                            <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
                            <a href="#!">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!-- Card content -->
                        <div class="card-body">

                            <!-- Title -->
                            <h4 class="card-title">Card title</h4>
                            <!-- Text -->
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <!-- Button -->
                            <!-- <a href="#" class="btn btn-primary">Button</a> -->
                            <a href="#"><span class="badge badge-primary">Read More&nbsp;<i class='fas fa-chevron-right'></i></span></a>

                        </div>

                        <!-- Card footer -->
                        <div class="card-footer text-muted text-center mt-4">
                        2 days ago
                        </div>

                    </div>
                    <!-- Card -->
                </div>

                <div class="col-md-4">
                    <div class="card mt-5">

                        <!-- Card image -->
                        <div class="view overlay">
                            <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
                            <a href="#!">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!-- Card content -->
                        <div class="card-body">

                            <!-- Title -->
                            <h4 class="card-title">Card title</h4>
                            <!-- Text -->
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <!-- Button -->
                            <a href="#"><span class="badge badge-primary">Read More&nbsp;<i class='fas fa-chevron-right'></i></span></a>

                        </div>

                        <!-- Card footer -->
                        <div class="card-footer text-muted text-center mt-4">
                        2 days ago
                        </div>

                    </div>
                    <!-- Card -->
                </div>

                <div class="col-md-4">
                    <div class="card mt-5">

                        <!-- Card image -->
                        <div class="view overlay">
                            <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
                            <a href="#!">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!-- Card content -->
                        <div class="card-body">

                            <!-- Title -->
                            <h4 class="card-title">Card title</h4>
                            <!-- Text -->
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <!-- Button -->
                            <a href="#"><span class="badge badge-primary">Read More&nbsp;<i class='fas fa-chevron-right'></i></span></a>

                        </div>

                        <!-- Card footer -->
                        <div class="card-footer text-muted text-center mt-4">
                        2 days ago
                        </div>

                        </div>
                        <!-- Card -->
                </div>

                <div class="col-md-4">
                    <div class="card mt-5">

                        <!-- Card image -->
                        <div class="view overlay">
                            <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
                            <a href="#!">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!-- Card content -->
                        <div class="card-body">

                            <!-- Title -->
                            <h4 class="card-title">Card title</h4>
                            <!-- Text -->
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <!-- Button -->
                            <!-- <a href="#" class="btn btn-primary">Button</a> -->
                            <a href="#"><span class="badge badge-primary">Read More&nbsp;<i class='fas fa-chevron-right'></i></span></a>

                        </div>

                        <!-- Card footer -->
                        <div class="card-footer text-muted text-center mt-4">
                        2 days ago
                        </div>

                    </div>
                    <!-- Card -->
                </div>

                <div class="col-md-4">
                    <div class="card mt-5">

                        <!-- Card image -->
                        <div class="view overlay">
                            <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
                            <a href="#!">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!-- Card content -->
                        <div class="card-body">

                            <!-- Title -->
                            <h4 class="card-title">Card title</h4>
                            <!-- Text -->
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <!-- Button -->
                            <a href="#"><span class="badge badge-primary">Read More&nbsp;<i class='fas fa-chevron-right'></i></span></a>

                        </div>

                        <!-- Card footer -->
                        <div class="card-footer text-muted text-center mt-4">
                        2 days ago
                        </div>

                    </div>
                    <!-- Card -->
                </div>

                <div class="col-md-4">
                    <div class="card mt-5">

                        <!-- Card image -->
                        <div class="view overlay">
                            <img class="card-img-top" src="https://mdbootstrap.com/img/Mockups/Lightbox/Thumbnail/img%20(67).jpg" alt="Card image cap">
                            <a href="#!">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <!-- Card content -->
                        <div class="card-body">

                            <!-- Title -->
                            <h4 class="card-title">Card title</h4>
                            <!-- Text -->
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <!-- Button -->
                            <a href="#"><span class="badge badge-primary">Read More&nbsp;<i class='fas fa-chevron-right'></i></span></a>

                        </div>

                        <!-- Card footer -->
                        <div class="card-footer text-muted text-center mt-4">
                        2 days ago
                        </div>

                        </div>
                        <!-- Card -->
                </div>

            </div>
        </div>
        <div class="col-md-4" >
            <div class="card mt-5" style="position: -webkit-sticky;position: sticky;">
                <div class="alert alert-primary" role="alert">
                    <h6 align="center">Pintar Yang Berawal Dari Mencontek</h6>
                </div>
                <div class="clearfix"></div>
                
                <!-- Classic tabs -->
                <div class="container">
                    <ul class="nav nav-tabs md-tabs" id="myTabEx" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" id="home-tab-ex" data-toggle="tab" href="#home-ex" role="tab" aria-controls="home-ex"
                            aria-selected="true">Tags</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab-ex" data-toggle="tab" href="#profile-ex" role="tab" aria-controls="profile-ex"
                            aria-selected="false">Populer</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab-ex" data-toggle="tab" href="#archive-ex" role="tab" aria-controls="profile-ex"
                            aria-selected="false">Archived</a>
                        </li>
                    </ul>
                    <div class="tab-content pt-5" id="myTabContentEx">
                        <div class="tab-pane fade active show" id="home-ex" role="tabpanel" aria-labelledby="home-tab-ex">
                            <ul>
                                <li><i class="fas fa-chevron-right"></i>&nbsp;Database</li>
                                <li><i class="fas fa-chevron-right"></i>&nbsp;Web Developer</li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="profile-ex" role="tabpanel" aria-labelledby="profile-tab-ex">
                            <ul>
                                <li><i class="fas fa-chevron-right"></i>&nbsp;Database</li>
                                <li><i class="fas fa-chevron-right"></i>&nbsp;Web Developer</li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="archive-ex" role="tabpanel" aria-labelledby="archive-tab-ex">
                            <ul>
                                <li><i class="fas fa-chevron-right"></i>&nbsp;2018</li>
                                <li><i class="fas fa-chevron-right"></i>&nbsp;2017</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Classic tabs -->

                <!-- member -->
                <div class="clearfix"></div>
                <div class="container mt-5">
                    <hr>
                    <h6 align="center"><b>MEMBER</b></h6>
                    <center><img src="<?= base_url()?>__assets/img/background/kancutnew.png" alt="thumbnail" class="img-thumbnail" style="width: 200px"></center>
                    <br>
                </div>
                <!-- end member -->
            </div>
        </div>
    </div>
</div>
<!-- end blog -->


<script>
$('.single-item').slick({
    infinite: true,
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
});
</script>
<?php $this->load->view('/templates/footer.php')?>