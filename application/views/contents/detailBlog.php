<title>{title}</title>
<?php $this->load->view('templates/header.php')?>

<div class="container" style="margin-top:100px">
    <div class="row">
        <div class="col-md-12">
            <img src="<?= base_url()?>__assets/img/slider/slider.jpg" alt="isi nya slug" width="100%" height="400px">
        </div>
    </div>
</div>

<div class="container mt-3">
    <h5 class="ml-3 title-detail-blog">All the world's waiting for you&nbsp;<span class="badge badge-green-cyan">Nov 17, 2020</span></h5>
    <p class="mt-3 text-justify">
    Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, 
    lorem quis bibendum auctor, nisi elit consequat ipsum, 
    nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. 
    Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. 
    Sed non mauris vitae erat consequat auctor eu in elit. 
    Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. 
    Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. 
    Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, 
    velit mauris egestas quam.
    Folly words widow one downs few age every seven. 
    If miss part by fact he park just shew. Discovered had get considered projection who favourable. 
    Necessary up knowledge it tolerably. Unwilling departure education is be dashwoods or an. Use off agreeable law unwilling sir deficient curiosity instantly.
    </p>
</div>

<div class="container mt-3">
    <div class="row">
        <div class="col-md-9">
            <span class="badge badge-primary">Database</span>
            <span class="badge badge-primary">Web Developer</span>
        </div>
        <div class="col-md-3">
            <span class="badge badge-dark text-right" style="float:right">By Hallochun</span>
        </div>
    </div>
    
    
</div>

<div class="container mt-3">
    <hr>
    <div class="row">
        <div class="col-md-1">
            <img src="<?= base_url()?>__assets/img/user/man.png" alt="user-profile" width="50px" height="50px">
        </div>
        <div class="col-md-8">
            <p class="title-name-detil">Adi Chandra Setiawan&nbsp;<span class="badge badge-green-cyan">2020-01-20</span></p>
            <p class="comment-content-detail">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            </p>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-1">
            <img src="<?= base_url()?>__assets/img/user/man.png" alt="user-profile" width="50px" height="50px">
        </div>
        <div class="col-md-8">
            <p class="title-name-detil">Adi Chandra Setiawan&nbsp;<span class="badge badge-green-cyan">2020-01-20</span></p>
            <p class="comment-content-detail">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
            </p>
        </div>
    </div>
    <h6 class="mt-3">Leave Comment</h6>
    <div class="container">
        <form method="post">
            <div class="row">
                <div class="col-md-9">
                    <textarea name="comment" id="comment" cols="20" rows="10" class="form-control" placeholder="Comment"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 mt-3">
                    <input type="text" name="fullname" id="fullname" class="form-control" placeholder="Fullname">
                </div>
                <div class="col-md-3 mt-3">
                    <input type="text" name="email" id="email" class="form-control" placeholder="Email">
                </div>
                <div class="col-md-3 mt-3">
                    <button class="btn btn-primary"><i class="fa fa-comment"></i>&nbsp;Comment</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view('/templates/footer.php')?>