<!DOCTYPE html>
<html lang="en" class="full-height">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Welcome To My Blog</title>
  <title></title>
  <link rel="shortcut icon" href="<?= base_url()?>__assets/hallochun.png" type="image/x-icon">
  <link rel="stylesheet" href="<?= base_url()?>__public/style.css">
  <link rel="stylesheet" href="<?= base_url()?>node_modules/mdbootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="<?= base_url()?>node_modules/mdbootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url()?>node_modules/mdbootstrap/css/mdb.css">
  <link rel="stylesheet" href="<?= base_url()?>node_modules/mdbootstrap/css/mdb.lite.css">
  <link rel="stylesheet" href="<?= base_url()?>node_modules/mdbootstrap/css/mdb.lite.min.css">
  <link rel="stylesheet" href="<?= base_url()?>node_modules/mdbootstrap/css/mdb.min.css">
  <link rel="stylesheet" href="<?= base_url()?>node_modules/mdbootstrap/css/style.css">

  <!-- slick -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.css">
  
  <script src="<?= base_url()?>node_modules/mdbootstrap/js/jquery.js"></script>
  <script src="<?= base_url()?>node_modules/mdbootstrap/js/jquery.min.js"></script>
  <script src="<?= base_url()?>node_modules/mdbootstrap/js/bootstrap.js"></script>
  <script src="<?= base_url()?>node_modules/mdbootstrap/js/bootstrap.min.js"></script>

  <script src='https://kit.fontawesome.com/a076d05399.js'></script>
  <!-- js slick -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
</head>
<body class="body">
  
  <!--Main Navigation-->
  <header>

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark dark-purple">
      <div class="container">
        <a class="navbar-brand" href="<?= site_url()?>"><img src="<?= base_url()?>__assets/hallochun.png" class="animated bounce infinite" width="50px" height="50px;" alt="https://hallochun.com"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active" id="home">
              <a class="nav-link" href="<?= base_url()?>">Home <span class="sr-only">(current)</span></a>
            </li>
            <!-- `<li class="nav-item" id="about">
              <a class="nav-link" href="#">About</a>
            </li>` -->
            <li class="nav-item" id="product">
              <a class="nav-link" href="<?= site_url()?>product">Product</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

  </header>
<!--Main Navigation-->

</body>
</html>

<script src="node_modules/mdbootstrap/js/mdb.js"></script>
<script src="node_modules/mdbootstrap/js/popper.js"></script>
<script src="node_modules/mdbootstrap/js/popper.min.js"></script>
